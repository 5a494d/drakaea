<?php

class PreguntaController  extends BaseController {
       // modificacion inicial 
       public function getEliminarRespuesta($id){
		$respuesta = Respuesta::find($id);
		$pregunta = $respuesta->pregunta_id;
		$respuesta->delete();
		return Redirect::to('/pregunta/mostrar/'.$pregunta);
	}
        public function postActualizar() {
		$pregunta_id = Input::get('pregunta_id');
		$pregunta = Pregunta::find($pregunta_id);
                $pregunta->pregunta = Input::get('pregunta');
                $pregunta->tema_id = Input::get('tema');
                $pregunta->save();
                return Redirect::to("/pregunta/mostrar/".$pregunta->id);
	}
	public function getMostrar($id) {
		$pregunta = Pregunta::find($id);
		return View::make('pregunta.mostrar')->with('pregunta', $pregunta);
	}
        // modificacion final
	public function getIndex() {
		$preguntas = Pregunta::all();
		return View::make('pregunta.index')->with('preguntas', $preguntas);
	}
	
	public function getAgregar() {
		$temas =  Tema::all();
		$tipoPreguntas = TipoPregunta::all();
		return View::make('pregunta.agregar')->with(array(
			'temas'=> $temas, 'tipoPreguntas'=> $tipoPreguntas));
	}
	public function postAgregar() {
		$pregunta = new Pregunta;
		$pregunta->pregunta = Input::get('pregunta');
		$pregunta->nivel = 1;
		$pregunta->tipo_pregunta_id = 2;
		$pregunta->tema_id = Input::get('tema');
		$pregunta->users_id = Auth::user()->id;
		$pregunta->save();
		return Redirect::to("/pregunta/mostrar/".$pregunta->id);
		//return Redirect::to('/pregunta/agregar-respuesta/'.$pregunta->id);
	}
	public function getAgregarRespuesta($id) {
		$pregunta = Pregunta::find($id);	
		return View::make('pregunta.agregarRespuesta')->with('pregunta',$pregunta);
	}
	public function postAgregarRespuesta() {

		$pregunta = Pregunta::find(Input::get('pregunta'));
		if (count($pregunta->respuestas) >= 6)
			return Redirect::to('/pregunta/mostrar/'.$pregunta->id)->with('message', 'no se puede registrar mas de 6 de respuesta'); 
			//return "no se puede registrar mas de 6 de respuesta "; 

		$respuesta = new Respuesta;
		$respuesta->es_corecta = false;
		if (Input::get('es-corecto')) 
			$respuesta->es_corecta = Input::get('es-corecto');
		$respuesta->respuesta = Input::get('respuesta');
		$respuesta->pregunta_id = Input::get('pregunta');
		$respuesta->save();
		return Redirect::to("/pregunta/mostrar/".$respuesta->pregunta_id);
		//return Redirect::to('/pregunta/agregar-respuesta/'.$respuesta->pregunta_id);
	}
	public function postActualizarRespuesta() {
		$respuesta_id = Input::get('respuesta_id');
		$respuesta = Respuesta::find($respuesta_id);
		
                $respuesta->es_corecta = false;
		if (Input::get('es-corecto')) 
			$respuesta->es_corecta = true;
		$respuesta->respuesta = Input::get('respuesta');
		$respuesta->save();
		return Redirect::to("/pregunta/mostrar/".$respuesta->pregunta->id);
	}
	
	public function getElegirRespuesta($id) {
		$pregunta = Pregunta::find($id);
		return View::make('pregunta.elegirRespuesta')->with('pregunta',$pregunta);
	}
	public function postElegirRespuesta() {
		$pregunta = Pregunta::find(Input::get('pregunta'));
		if($pregunta->TipoPregunta->nombre == 'single') {
			$id_corecto = Input::get('es-corecto');
			foreach($pregunta->respuestas as $respuesta) {
				if($respuesta->id == $id_corecto) {
					$respuesta->es_corecta = true;
				} else {
					$respuesta->es_corecta = false;
				}
				$respuesta->save();
					
			}
		} else {
			foreach($pregunta->respuestas as $respuesta) {
				$respuesta->es_corecta = false;
				$respuesta->save();
			}
			$ids_corectos = Input::get('respuestas');
			foreach($ids_corectos as $id) {
				$respuesta = Respuesta::find($id);
				$respuesta->es_corecta = true;
				$respuesta->save();
			}
		}
		return Redirect::to('/pregunta');
	}
	
}
