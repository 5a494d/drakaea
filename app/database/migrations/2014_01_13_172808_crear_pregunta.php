<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearPregunta extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pregunta', function(Blueprint $table)
		{
			$table->create();
			$table->increments('id');
			$table->string('pregunta');
			$table->integer('nivel');
			$table->integer('tipo_pregunta_id');
			$table->integer('tema_id');
			$table->foreign('tipo_pregunta_id')->references('id')->on('tipo_pregunta');
			$table->foreign('tema_id')->references('id')->on('tema');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pregunta', function(Blueprint $table)
		{
			Schema::drop('pregunta');
		});
	}

}
