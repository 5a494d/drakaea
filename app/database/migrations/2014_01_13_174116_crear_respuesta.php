<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearRespuesta extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('respuesta', function(Blueprint $table)
		{
			$table->create();
			$table->increments('id');
			$table->integer('pregunta_id');
			$table->string('respuesta');
			$table->boolean('es_corecta');
			$table->foreign('pregunta_id')->references('id')->on('pregunta')->onDelete('cascade');
		});
		Schema::table('respuesta', function($table){
			$table->dropPrimary('respuesta_id_primary');
			$table->primary(array('id', 'pregunta_id'));
		});
		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('respuesta', function(Blueprint $table)
		{
			Schema::drop('respuesta');
		});
	}

}
