<?php

class Respuesta extends Eloquent {
	protected $table = 'respuesta';
	public $timestamps = false;
	
	public function pregunta(){
		return $this->belongsTo('Pregunta');
	}
}
