<?php

class Tema extends Eloquent {
	protected $table = 'tema';
	public $timestamps = false;
	
	public function Preguntas() {
		return $this->hasMany('Pregunta');
	}
}
