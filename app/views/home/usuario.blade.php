<!doctype html>
<html>
<head>
    <title> mi primera pagina </title>

    <style>
        body {
            background : white;
        }
        header h1, header h2 {
            display: inline;
        }
        header h1 a {
            color : #9a3b4f;
            font-size : 65px;
             
        }
        .blue {
            color : blue;
        }
        .green {
            color : green;
        }
        .red {
            color : red;
        }
        .registar-pregunta form * {
            color : #9a3b4f;
            border-color : #6F9D03;
            background : white;
        }
        .registar-pregunta form label {
	    color : #6F9D03;
        }
        .preguntas ul li a {
            color : #9a3b4f;
        }
        .preguntas h2 {
            color : #6F9D03;
        }	
    </style>
</head>
<body>

<header>
    <h1> <a href="/">drakaea</a></h1>
    <h2> <span class="blue">{</span> <span class="green">O</span> <span class="red">I</span> <span class="blue">}</span></h2>
</header>

<div class="registar-pregunta">
<form method="post" action="/pregunta/agregar">
    <label> pregunta </label>
    <input name="pregunta" type="text" placeholder="ingrease la pregunta">
    <label> tema</label>
    <select name="tema">
	@foreach(Tema::all() as $tema)
	     <option value="{{$tema->id}}">{{$tema->nombre}}</option>
	@endforeach
    </select>
    <input type="submit" value="registrar">
</form>
</div>
<div class="preguntas">
    <h2> Preguntas que aporte :</h2>
    <ul>
    @foreach(Auth::user()->preguntas as $pregunta)
        <li><a href="/pregunta/mostrar/{{$pregunta->id}}">{{$pregunta->pregunta}}</a></li>
    @endforeach
    </ul>
</div>
</body>
</html>
