<!doctype html>
<html>
<head>
    <title> mi primera pagina </title>

    <style>
        body {
            background : white;
        }
        header h1, header h2 {
            display: inline;
        }
        header h1 a {
            color : #9a3b4f;
            font-size : 65px;
             
        }
        .blue {
            color : blue;
        }
        .green {
            color : green;
        }
        .red {
            color : red;
        }
        .preguntas form * {
            color : #9a3b4f;
            border-color :#6F9D03;
            background : white; 
        }
        
        .preguntas h2 {
             color : #6F9D03;
             
        }
	.messag {
		border : 3px solid red;
		border-radius : 10px;
		padding : 4px;
		background : white;
		color : red;
		font-size : 20px;
		text-align : center;
		font-weight : bold;
		-moz-transition : visibility 0s 2s, opacity 2s linear;
	}
	.message-out {
		display : none;
	}

	@keyframes ocultar-alerta {
  		0% {
		border : 3px solid red;
		border-radius : 10px;
		padding : 4px;
		background : white;
		color : red;
		font-size : 20px;
		text-align : center;
		font-weight : bold;
  		}
  		100% {
		border : 3px solid red;
		border-radius : 10px;
		display : none;
		visibility : hidden;
		color : white;
		border-color : white
		font-size : 0px;
  		}
	}
	
	.message {
		animation-name: ocultar-alerta;
 animation-duration: 5s;
 animation-iteration-count: 1;
 animation-direction: alternate;
 animation-timing-function: ease-out;
 animation-fill-mode: forwards;
	}
	
    </style>
    <script>
	//window.setTimeout(ocultar, 3000);
	function ocultar () {
		document.getElementById("message").className = "message-out";
	}
    </script>
</head>

<body>

<header>
    <h1> <a href="/">drakaea</a></h1>
    <h2> <span class="blue">{</span> <span class="green">O</span> <span class="red">I</span> <span class="blue">}</span></h2>
</header>

</div>
<div class="preguntas">
            <form method="post" action="/pregunta/actualizar">
                <input name="pregunta_id" type="hidden" value="{{$pregunta->id}}">
                <input name="pregunta" type="text" value="{{$pregunta->pregunta}}">
                <select name="tema">
	        @foreach(Tema::all() as $tema)
	           <option value="{{$tema->id}}" @if($tema == $pregunta->tema) selected @endif > {{$tema->nombre}}</option>
	        @endforeach
                </select>
                <input type="submit" value="actualizar">
           </form>
            <h2> Respuestas :</h2>
            
@if(Session::get('message'))
<div class='message' id="message">
	<p>{{Session::get('message')}}</p>
</div>
@endif
            <ul>
            @foreach($pregunta->respuestas as $respuesta)
                <li>       
	    <form method="post" action="/pregunta/actualizar-respuesta">
                <input type="hidden" name="pregunta" value="{{$pregunta->id}}">
                <input type="hidden" name="respuesta_id" value="{{$respuesta->id}}">
                <input type="text" name="respuesta" placeholder="ingrese la respuesta" value="{{$respuesta->respuesta}}">
                <input type="checkbox" name="es-corecto" @if ($respuesta->es_corecta) checked @endif >        
                <input type="submit" value="actualizar">
                <a href="/pregunta/eliminar-respuesta/{{$respuesta->id}}">eliminar</a>
                </form>
                </li>
            @endforeach
            </ul>
	    <form method="post" action="/pregunta/agregar-respuesta">
                <input type="hidden" name="pregunta" value="{{$pregunta->id}}">
                <input type="text" name="respuesta" placeholder="ingrese la respuesta">
                <input type="checkbox" name="es-corecto">
                <input type="submit" value="agregar">
            </form>
</div>
</body>
</html>
