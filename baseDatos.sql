drop table respuesta;
drop table pregunta;
drop table tema;
drop table users;
create table users(
    id serial primary key,
    email varchar(255),
    password varchar(255)
);

create table tema(
    id integer primary key,
    nombre varchar(50),
    identificador integer
);
insert into tema values(1, 'instalacion' , 1);
insert into tema values(2, 'bash' , 2);

create table pregunta(
    id serial primary key,
    nivel integer,
    pregunta varchar(200),
    tipo_pregunta_id integer ,
    users_id integer references users(id),
    tema_id integer references tema(id)
);
create table respuesta(
    id serial primary key,
    respuesta varchar(200),
    es_corecta boolean ,
    pregunta_id integer references pregunta(id)
);

